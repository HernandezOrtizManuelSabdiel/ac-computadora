# Computadoras

Se presentaran dos mapas conceptuales sobre el desarrollo de la computadora


## Von-Neumann vs Hardvard

```plantuml
@startmindmap
*[#lightcoral] Von-Neumann VS\nHardvard
** Von-Neumann
***[#lightblue] Modelo
**** Caracteristicas
*****[#lightblue] Unico espacio de memoria, que contienen tanto las instrucciones como los datos.
*****[#lightblue] El espacio es accesible por posicion, sin importar si se accede a datos o instrucciones
*****[#lightblue] La ejecucion de las instrucciones es secuencial

**** Arquitectura
*****_ se divide en 4 partes 
******[#lightblue] Procesador
******[#lightblue] Dispositivos de almacenamiento
******[#lightblue] Dispositivos de entrada y salida (E/S)
******[#lightblue] Buses
*******_ de
******** Datos
******** Direccion
******** Control

***[#lightblue] Historia
**** Nacio un 28 de diciembre de 1903
**** Modifico la ENIAC para que se ejecutara como una maquina de programa almacenado
**** Presiono al instituto de Estudios Avanzados para contruir una computadora mejorada (IAS)
**** La maquina IAS utilizaba aritmetica binaria, mientras que la ENIAC numeros decimales
**** Propuso la adopcion del BIT como medida de la memoria de los ordenadores (0 y 1)
**** Fallecio un 8 de febrero de 1957

** Hardvard
***[#lightblue] Historia
**** Fue desarrollada por Howard Aiken en 1944
**** Se nombra de esta manera por la computadora Hardvard Mark

***[#lightblue] Modelo
**** Caracteristicas
*****[#lightblue] Puede contener diferente informacion en la misma ubicacion
*****[#lightblue] El CPU podia acceder a la vez en ambas memorias
*****[#lightblue] Contenia dos memorias independientes \ncon diferente proposito
****** Almacenar instrucciones
****** Almacenar datos

**** Arquitectura
*****[#lightblue] Procesador
*****[#lightblue] Memoria de instrucciones
*****[#lightblue] Memoria de datos
*****[#lightblue] Buses independientes de cada memoria

@endmindmap
```
## Supercomputadoras en Mexico


```plantuml
@startmindmap
*[#lightcoral] Supercomputadoras en Mexico
** Ordenadores con \ngran capasidad a la hora de \n calcular cientos de operaciones
***[#lightblue] Manejo
****_ se necesitan \nciertos
***** Cuidados
******[#lightblue] Monitoreo de la temperatura para evitar sobrecalentamiento
******[#lightblue] Plantas de energia de reseva por emergencia
******[#lightblue] Prevencion de cualquier incidente

**** Practico
*****[#lightblue] Capacidad para realizar simulaciones
*****[#lightblue] Investigaciones cientificas
****** Formacion de una estrella 
****** Avance de un huracan
****** Estructura del ADC para entender su composicion
*****[#lightblue] Analisis de riesgos financieros
*****[#lightblue] Prevencion de desastres naturales
*****[#lightblue] Procesar animaciones de peliculas 

***[#lightblue] Historia 
**** 1958
*****[#lightblue] Inicia la era de computacion en Mexico. \nSe adquiere la IBM 650
**** 1991 
*****[#lightblue] Adquisicion por parte de la UNAM de la \nprimer supercomputadora en America Latina \n CRAY 432
**** 2007 
*****[#lightblue] Realiza sus primeras operaciones la supercomputadora KanBalam
**** 2012
*****[#lightblue] Se pone en funcionamiento la supercomputadora Miztli
*****[#lightblue] Creacion de LANCAD en colaboracion de UNAM, \nUAM, Y CINVESTAV
**** 2013
*****[#lightblue] Iniciativa por parte de la BUAP para la creacion de su supercomputadora

@endmindmap
```



